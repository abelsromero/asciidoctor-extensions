package com.puravida.asciidoctor;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
/**
 * Created by jorge on 15/07/17.
 */
public class TooltipTests {

    @Test
    public void postProcessorIsApplied() {

        Options options = new Options();
        options.setHeaderFooter(true);
        options.setSafe(SafeMode.SERVER);

        Asciidoctor asciidoctor = Asciidoctor.Factory.create();
        String converted = asciidoctor.convert(String.format(
                "= My document\n" +
                        ":toc: left\n" +
                        ":toc-collapsable: \n" +
                        "== Hola \n"+
                        "caracola \n"+
                        "[#bloque1,source]\n"+
                        "----\n"+
                        "this text //<1>\n"+
                        "----\n"+
                        "<1>This text is the tooltip to this text\n"+
                        "----\n"+
                        ""), options);
        System.out.println(converted);
        assertThat(converted).contains("tooltip");
    }

}
