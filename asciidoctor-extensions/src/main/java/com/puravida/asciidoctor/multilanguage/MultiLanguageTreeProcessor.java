package com.puravida.asciidoctor.multilanguage;

import org.asciidoctor.ast.Block;
import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.Treeprocessor;
import com.puravida.asciidoctor.ReadResources;
import org.asciidoctor.ast.Document;
import org.asciidoctor.extension.Postprocessor;

import java.io.File;
import java.util.List;
import java.util.Map;


public class MultiLanguageTreeProcessor extends Treeprocessor {

    public MultiLanguageTreeProcessor(Map<String, Object> config) {
        super(config);
    }

    private Document document;

    @Override
    public Document process(Document document) {

        if("html5".equals(document.getAttributes().get("backend")))
            return document;

        String code = (String)(document.getAttributes().get(MultiLanguagePostProcessor.TAG));
        if( code == null)
            return document;

        String[]languages = code.split(",");
        if(languages.length==0)
            return document;

        String defaultLang = languages[0];
        this.document = document;

        final List<StructuralNode> blocks = this.document.getBlocks();

        for (int i = 0; i < blocks.size(); i++) {
            final StructuralNode block = blocks.get(i);
            for(String l : languages) {
                if( l != defaultLang && block.hasRole("language-"+l) ){
                    blocks.remove(block);
                }
            }
        }

        return document;
    }
}
