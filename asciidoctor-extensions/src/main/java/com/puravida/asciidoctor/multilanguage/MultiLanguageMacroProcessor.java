package com.puravida.asciidoctor.multilanguage;

import com.puravida.asciidoctor.ReadResources;
import org.asciidoctor.ast.Block;
import org.asciidoctor.ast.StructuralNode;
import org.asciidoctor.extension.BlockMacroProcessor;

import java.util.Arrays;
import java.util.Map;

public class MultiLanguageMacroProcessor extends BlockMacroProcessor {

    public static final String TAG = "multilanguage";

    public MultiLanguageMacroProcessor(String macroName, Map<String, Object> config) {
        super(macroName, config);
    }

    @Override
    public Object process(StructuralNode parent, String target, Map<String, Object> attributes) {

        String content = "";

        if( target != null || target.trim() != ""){
            content = buildToolbar(target);
        }else {
            if (parent.getDocument().getAttributes().containsKey(MultiLanguagePostProcessor.TAG)) {
                String lang = parent.getDocument().getAttributes().get(MultiLanguagePostProcessor.TAG).toString();
                content = buildToolbar(lang);
            }
        }
        return createBlock(parent, "pass", Arrays.asList(content.toString()), attributes);
    }

    public static String buildToolbar(String lang){
        StringBuffer content = new StringBuffer("<span>");
        String[]languages = lang.split(",");
        for(String l : languages) {
            content.append("<span><img src=\"blank.gif\" class=\"flag flag-"+l+"\" alt=\""+l+"\" /></span>");
        }
        content.append("</span>");
        return content.toString();
    }

}
