package com.puravida.asciidoctor;

import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Options;
import org.asciidoctor.SafeMode;
import org.junit.Test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

public class WriteFileQuizTests {

    @Test
    public void fullTest() throws Exception {

        Options options = new Options();
        options.setHeaderFooter(true);
        options.setSafe(SafeMode.SERVER);

        Asciidoctor asciidoctor = Asciidoctor.Factory.create();
        String adoc = new String(
                Files.readAllBytes(Paths.get(getClass().getResource("/quiz1.adoc").toURI())));
        PrintWriter os = new PrintWriter(new FileOutputStream("build/quiz1.adoc"));
        os.println(adoc);
        os.flush();
        os.close();

        asciidoctor.convertFile(new File("build/quiz1.adoc"), options);

    }

}
