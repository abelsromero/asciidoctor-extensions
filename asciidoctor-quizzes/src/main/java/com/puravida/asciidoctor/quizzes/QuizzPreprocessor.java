package com.puravida.asciidoctor.quizzes;

import com.puravida.asciidoctor.quizzes.model.Quiz;
import org.asciidoctor.ast.Document;
import org.asciidoctor.extension.Preprocessor;
import org.asciidoctor.extension.PreprocessorReader;

import java.util.Map;

public class QuizzPreprocessor extends Preprocessor {

    public static final String TAG = "quiz";

    @Override
    public void process(Document document, PreprocessorReader reader) {
        Quiz quiz = new Quiz();
        document.getAttributes().put(TAG,quiz);
    }
}
