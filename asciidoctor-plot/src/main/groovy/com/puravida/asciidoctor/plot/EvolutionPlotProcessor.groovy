package com.puravida.asciidoctor.plot

import com.puravida.plot.PlotBuilder

class EvolutionPlotProcessor extends AbstractPlotProcessor{

    EvolutionPlotProcessor(String name, Map<String, Object> config) {
        super(name,config)
    }

    ByteArrayOutputStream executeClosure(Closure closure){
        PlotBuilder.instance.evolutionPlot closure
    }

}
