package com.puravida.asciidoctor

import com.puravida.asciidoctor.plot.BarPlotProcessor
import com.puravida.asciidoctor.plot.EvolutionPlotProcessor
import com.puravida.asciidoctor.plot.GnuPlotProcessor
import com.puravida.asciidoctor.plot.LinePlotProcessor
import org.asciidoctor.Asciidoctor
import org.asciidoctor.extension.spi.ExtensionRegistry

class AsciidoctorPlotExtensionRegistry implements ExtensionRegistry{

    @Override
    void register(Asciidoctor asciidoctor) {

        asciidoctor.javaExtensionRegistry().block 'lineplot', LinePlotProcessor

        asciidoctor.javaExtensionRegistry().block 'barplot', BarPlotProcessor

        asciidoctor.javaExtensionRegistry().block 'evolutionplot', EvolutionPlotProcessor

        asciidoctor.javaExtensionRegistry().block 'gnuplot', GnuPlotProcessor
    }
}
