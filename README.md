# asciidoctor-extensions

asciidoctor-extensions are ... some extensions for @asciidoctor

## Live example

You can watch a full demo with all extensions at https://puravida-software.gitlab.io/asciidoctor-extensions/

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites


### Installing

## Running the tests


## Deployment


## Built With

* [gradle](http://www.gradle.org/) - automation task management
* [Asciidoctor](https://asciidoctor.org/) 

## Contributing


## Versioning

## Authors

* **Jorge Aguilera** - *Initial work* - [@jagedn](https://twitter.com/jagedn)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

